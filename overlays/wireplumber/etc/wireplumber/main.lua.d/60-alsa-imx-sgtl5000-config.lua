-- Force audio format not setup correctly by default
table.insert(alsa_monitor.rules, {
  matches = {
    {
      { "alsa.driver_name", "equals", "snd_soc_imx_sgtl5000" }
    }
  },
  apply_properties = {
    ["audio.format"] = "S16LE",
  }
})

-- Set priority to force output through audio device instead of HDMI output
table.insert(alsa_monitor.rules, {
  matches = {
    {
      { "node.name", "matches", "alsa_output.*" },
      { "alsa.driver_name", "equals", "snd_soc_imx_sgtl5000" }
    }
  },
  apply_properties = {
    ["priority.driver"] = 1100,
    ["priority.session"] = 1100,
  }
})
